resource "aws_lb" "api" {
  name               = "${local.prefix}-api"
  load_balancer_type = "application"

  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [aws_security_group.lb.id]

  tags = merge(
    local.common_tags,
    {
      Name = "${local.prefix}-api"
    }
  )
}

resource "aws_lb_target_group" "api" {
  name        = "${local.prefix}-api"
  port        = 8000
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"


  health_check {
    path = "/admin/login/"
  }

  tags = merge(
    local.common_tags,
    {
      Name = "${local.prefix}-api"
    }
  )
}

resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    # type = "redirect"
    # redirect {
    #   port        = "443"
    #   protocol    = "HTTPS"
    #   status_code = "HTTP_301"
    # }
    target_group_arn = aws_lb_target_group.api.arn
    type             = "forward"
  }
}

# Create certificate for the load balancer
# resource "aws_acm_certificate" "api" {
#   domain_name       = aws_lb.api.dns_name
#   validation_method = "DNS"

#   tags = merge(
#     local.common_tags,
#     {
#       Name = "${local.prefix}-api"
#     }
#   )
# }

# # Create SSL listener for the load balancer
# resource "aws_lb_listener" "api_https" {
#   load_balancer_arn = aws_lb.api.arn
#   port              = 443
#   protocol          = "HTTPS"
#   ssl_policy        = "ELBSecurityPolicy-2016-08"


#   certificate_arn = aws_acm_certificate.api.arn

#   default_action {
#     target_group_arn = aws_lb_target_group.api.arn
#     type             = "forward"
#   }
# }



resource "aws_security_group" "lb" {
  name        = "${local.prefix}-lb"
  description = "Allow access to the load balancer"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  ingress {
    protocol  = "tcp"
    from_port = 443
    to_port   = 443
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }

  egress {
    protocol  = "tcp"
    from_port = 8000
    to_port   = 8000
    cidr_blocks = [
      "0.0.0.0/0"
    ]
  }
}