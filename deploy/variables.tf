variable "prefix" {
  default = "recipe-app"
}

variable "project" {
  default = "recipe-app-devops"
}

variable "contact" {
  default = "Alex"
}

variable "db_username" {
  description = "The username for the database"
}

variable "db_password" {
  description = "The password for the database"
}

variable "bastion_key_name" {
  default = "bastion"
}

variable "ecr_image_api" {
  default = "088976404449.dkr.ecr.us-east-1.amazonaws.com/recipe-app:latest"
}

variable "ecr_image_proxy" {
  default = "088976404449.dkr.ecr.us-east-1.amazonaws.com/nginx-proxy:latest"
}

variable "django_secret_key" {
  description = "The secret key for Django"
}